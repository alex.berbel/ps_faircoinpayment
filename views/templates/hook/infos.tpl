{*
* 2018 Al-Demon for KOMUN based in PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Al-Demon <al-demon@3toques.es>
*  @copyright  2018 Al-Demon for KOMUN based in PrestaShop
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA and KOMUN is free like the wind
*}

<div class="alert alert-info">
<img src="../modules/faircoinpayment/logo.png" style="float:left; margin-right:15px;" height="60">
<p><strong>{l s="This module allows you to accept secure payments by faircoin wallet." mod='faircoinpayment'}</strong></p>
<p>{l s="If the client chooses to pay with faircoin, the order status will change to 'Waiting for Payment'." mod='faircoinpayment'}</p>
<p>{l s="That said, you must manually confirm the order upon receiving the fairs." mod='faircoinpayment'}</p>
<p>{l s="In Faircoin address you need to put your faircoin wallet address, the payments go there." mod='faircoinpayment'}</p>
</div>
