{*
* 2018 Al-Demon for KOMUN based in PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Al-Demon <al-demon@3toques.es>
*  @copyright  2018 Al-Demon for KOMUN based in PrestaShop
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA and KOMUN is free like the wind
*}

{if $status == 'ok'}
    <p>
      {l s='Your order on %s is complete.' sprintf=[$shop_name] mod='faircoinpayment'}<br/>
      {l s='Please pay us with your faircoin wallet:' mod='faircoinpayment'}
    </p>
<div class="container">
 <div class="row">
 <div class="col-sm-6">
	<img src="../modules/faircoinpayment/faircoin.png" alt="{l s='Pay with faircoin' mod='faircoinpayment'}" width="133" height="200"/>
	<p>{l s='Faircoin Address:' mod='faircoinpayment'} {$faircoinAddress nofilter}</p>
 </div>
 <div class="col-sm-6">
	<img src="https://api.qrserver.com/v1/create-qr-code/?data={$fairconAddress nofilter}&size=200x200">
 </div>
 </div>
<br>
</div>
    {include file='module:faircoinpayment/views/templates/hook/_partials/payment_infos.tpl'}

    <p>
      {l s='Please specify your order reference %s in the faircoin description.' sprintf=[$reference] mod='faircoinpayment'}<br/>
      {l s='We\'ve also sent you this information by e-mail.' mod='faircoinpayment'}
    </p>
    <strong>{l s='Your order will be sent as soon as we receive payment.' mod='faircoinpayment'}</strong>
    <p>
      {l s='If you have questions, comments or concerns, please contact our [1]expert customer support team[/1].' mod='faircoinpayment' sprintf=['[1]' => "<a href='{$contact_url}'>", '[/1]' => '</a>']}
    </p>
{else}
    <p class="warning">
      {l s='We noticed a problem with your order. If you think this is an error, feel free to contact our [1]expert customer support team[/1].' mod='faircoinpayment' sprintf=['[1]' => "<a href='{$contact_url}'>", '[/1]' => '</a>']}
    </p>
{/if}
