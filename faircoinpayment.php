<?php
/*
* 2018 Al-Demon for KOMUN based in PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Al-Demon <al-demon@3toques.es>
*  @copyright  2018 Al-Demon for KOMUN based in PrestaShop
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA and KOMUN is free like the wind
*/

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

if (!defined('_PS_VERSION_')) {
    exit;
}

class Faircoinpayment extends PaymentModule
{
    const FLAG_DISPLAY_PAYMENT_INVITE = 'FAIRCOIN_PAYMENT_INVITE';

    protected $_html = '';
    protected $_postErrors = array();

    public $address;
    public $extra_mail_vars;

    public function __construct()
    {
        $this->name = 'faircoinpayment';
        $this->tab = 'payments_gateways';
        $this->version = '1.0.1';
        $this->ps_versions_compliancy = array('min' => '1.7.1.0', 'max' => _PS_VERSION_);
        $this->author = 'Al-Demon';
        $this->controllers = array('payment', 'validation');
        $this->is_eu_compatible = 1;

        $this->currencies = true;
        $this->currencies_mode = 'checkbox';

        $config = Configuration::getMultiple(array('FAIRCOIN_ADDRESS', 'FAIRCOIN_RESERVATION_DAYS'));
        if (!empty($config['FAIRCOIN_ADDRESS'])) {
            $this->address = $config['FAIRCOIN_ADDRESS'];
        }
        if (!empty($config['FAIRCOIN_RESERVATION_DAYS'])) {
            $this->reservation_days = $config['FAIRCOIN_RESERVATION_DAYS'];
        }

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('Faircoin payment', array(), 'Modules.Faircoinpayment.Admin');
        $this->description = $this->l('Accept payments by faircoin wallet.', array(), 'Modules.Faircoinpayment.Admin');
        $this->confirmUninstall = $this->l('Are you sure about removing these details?', array(), 'Modules.Faircoinpayment.Admin');

        if (!count(Currency::checkPaymentCurrencies($this->id))) {
            $this->warning = $this->l('No currency has been set for this module.', array(), 'Modules.Faircoinpayment.Admin');
        }

        $this->extra_mail_vars = array(
                                        '{faircoin_address}' => nl2br(Configuration::get('FAIRCOIN_ADDRESS')),
                                        );
    }

    public function install()
    {
        Configuration::updateValue(self::FLAG_DISPLAY_PAYMENT_INVITE, true);
        if (!parent::install() || !$this->registerHook('paymentReturn') || !$this->registerHook('paymentOptions')) {
            return false;
        }
        return true;
    }

    public function uninstall()
    {
        $languages = Language::getLanguages(false);
        foreach ($languages as $lang) {
            if (!Configuration::deleteByName('FAIRCOIN_CUSTOM_TEXT', $lang['id_lang'])) {
                return false;
            }
        }

        if (!Configuration::deleteByName('FAIRCOIN_ADDRESS')
                || !Configuration::deleteByName('FAIRCOIN_RESERVATION_DAYS')
                || !Configuration::deleteByName(self::FLAG_DISPLAY_PAYMENT_INVITE)
                || !parent::uninstall()) {
            return false;
        }
        return true;
    }

 protected function _postValidation()
    {
        if (Tools::isSubmit('btnSubmit')) {
            Configuration::updateValue(self::FLAG_DISPLAY_PAYMENT_INVITE,
                Tools::getValue(self::FLAG_DISPLAY_PAYMENT_INVITE));

	}

    }

    protected function _postProcess()
    {
        if (Tools::isSubmit('btnSubmit')) {
            Configuration::updateValue('FAIRCOIN_ADDRESS', Tools::getValue('FAIRCOIN_ADDRESS'));

            $custom_text = array();
            $languages = Language::getLanguages(false);
            foreach ($languages as $lang) {
                if (Tools::getIsset('FAIRCOIN_CUSTOM_TEXT_'.$lang['id_lang'])) {
                    $custom_text[$lang['id_lang']] = Tools::getValue('FAIRCOIN_CUSTOM_TEXT_'.$lang['id_lang']);
                }
            }
            Configuration::updateValue('FAIRCOIN_RESERVATION_DAYS', Tools::getValue('FAIRCOIN_RESERVATION_DAYS'));
            Configuration::updateValue('FAIRCOIN_CUSTOM_TEXT', $custom_text);
        }
        $this->_html .= $this->displayConfirmation($this->l('Settings updated', array(), 'Admin.Global'));
    }

    protected function _displayFairCoin()
    {
        return $this->display(__FILE__, 'infos.tpl');
    }

    public function getContent()
    {
        if (Tools::isSubmit('btnSubmit')) {
            $this->_postValidation();
            if (!count($this->_postErrors)) {
                $this->_postProcess();
            } else {
                foreach ($this->_postErrors as $err) {
                    $this->_html .= $this->displayError($err);
                }
            }
        } else {
            $this->_html .= '<br />';
        }

        $this->_html .= $this->_displayFairCoin();
        $this->_html .= $this->renderForm();

        return $this->_html;
    }

    public function hookPaymentOptions($params)
    {
        if (!$this->active) {
            return;
        }

        if (!$this->checkCurrency($params['cart'])) {
            return;
        }

        $this->smarty->assign(
            $this->getTemplateVarInfos()
        );

        $newOption = new PaymentOption();
        $newOption->setModuleName($this->name)
                ->setCallToActionText($this->l('Pay with faircoin', array(), 'Modules.Faircoinpayment.Shop'))
                ->setAction($this->context->link->getModuleLink($this->name, 'validation', array(), true))
                ->setAdditionalInformation($this->fetch('module:faircoinpayment/views/templates/hook/faircoinpayment_intro.tpl'));
        $payment_options = [
            $newOption,
        ];

        return $payment_options;
    }

    public function hookPaymentReturn($params)
    {
        if (!$this->active || !Configuration::get(self::FLAG_DISPLAY_PAYMENT_INVITE)) {
            return;
        }

        $state = $params['order']->getCurrentState();
        if (
            in_array(
                $state,
                array(
                    Configuration::get('PS_OS_BANKWIRE'),
                    Configuration::get('PS_OS_OUTOFSTOCK'),
                    Configuration::get('PS_OS_OUTOFSTOCK_UNPAID'),
                )
        )) {
            $faircoinAddress = Tools::nl2br($this->address);
            if (!$faircoinAddress) {
                $faircoinAddress = '___________';
            }

            $this->smarty->assign(array(
                'shop_name' => $this->context->shop->name,
                'total' => Tools::displayPrice(
                    $params['order']->getOrdersTotalPaid(),
                    new Currency($params['order']->id_currency),
                    false
                ),
                'faircoinAddress' => $faircoinAddress,
                'status' => 'ok',
                'reference' => $params['order']->reference,
                'contact_url' => $this->context->link->getPageLink('contact', true)
            ));
        } else {
            $this->smarty->assign(
                array(
                    'status' => 'failed',
                    'contact_url' => $this->context->link->getPageLink('contact', true),
                )
            );
        }

        return $this->fetch('module:faircoinpayment/views/templates/hook/payment_return.tpl');
    }

    public function checkCurrency($cart)
    {
        $currency_order = new Currency($cart->id_currency);
        $currencies_module = $this->getCurrency($cart->id_currency);

        if (is_array($currencies_module)) {
            foreach ($currencies_module as $currency_module) {
                if ($currency_order->id == $currency_module['id_currency']) {
                    return true;
                }
            }
        }
        return false;
    }

    public function renderForm()
    {
        $fields_form = array(
            'form' => array(
                'input' => array(
                    array(
                        'type' => 'textarea',
                        'label' => $this->l('Faircoin address', array(), 'Modules.Faircoinpayment.Admin'),
                        'name' => 'FAIRCOIN_ADDRESS',
                        'required' => true
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save', array(), 'Admin.Actions'),
                )
            ),
        );
        $fields_form_customization = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Customization', array(), 'Modules.Faircoinpayment.Admin'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Reservation period', array(), 'Modules.Faircoinpayment.Admin'),
                        'desc' => $this->l('Number of days the items remain reserved', array(), 'Modules.Faircoinpayment.Admin'),
                        'name' => 'FAIRCOIN_RESERVATION_DAYS',
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => $this->l('Information to the customer', array(), 'Modules.Faircoinpayment.Admin'),
                        'name' => 'FAIRCOIN_CUSTOM_TEXT',
                        'desc' => $this->l('Information on the fair transfer (processing time, starting of the shipping... )', array(), 'Modules.Faircoinpayment.Admin'),
                        'lang' => true
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display the invitation to pay in the order confirmation page', array(), 'Modules.Faircoinpayment.Admin'),
                        'name' => self::FLAG_DISPLAY_PAYMENT_INVITE,
                        'is_bool' => true,
                        'hint' => $this->l('Your country\'s legislation may require you to send the invitation to pay by email only. Disabling the option will hide the invitation on the confirmation page.', array(), 'Modules.Faircoinpayment.Admin'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled', array(), 'Admin.Global'),
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled', array(), 'Admin.Global'),
                            )
                        ),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save', array(), 'Admin.Actions'),
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? : 0;
        $this->fields_form = array();
        $helper->id = (int)Tools::getValue('id_carrier');
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'btnSubmit';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='
            .$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form, $fields_form_customization));
    }

    public function getConfigFieldsValues()
    {
        $custom_text = array();
        $languages = Language::getLanguages(false);
        foreach ($languages as $lang) {
            $custom_text[$lang['id_lang']] = Tools::getValue(
                'FAIRCOIN_CUSTOM_TEXT_'.$lang['id_lang'],
                Configuration::get('FAIRCOIN_CUSTOM_TEXT', $lang['id_lang'])
            );
        }

        return array(
            'FAIRCOIN_ADDRESS' => Tools::getValue('FAIRCOIN_ADDRESS', Configuration::get('FAIRCOIN_ADDRESS')),
            'FAIRCOIN_RESERVATION_DAYS' => Tools::getValue('FAIRCOIN_RESERVATION_DAYS', Configuration::get('FAIRCOIN_RESERVATION_DAYS')),
            'FAIRCOIN_CUSTOM_TEXT' => $custom_text,
            self::FLAG_DISPLAY_PAYMENT_INVITE => Tools::getValue(self::FLAG_DISPLAY_PAYMENT_INVITE,
                Configuration::get(self::FLAG_DISPLAY_PAYMENT_INVITE))
        );
    }

    public function getTemplateVarInfos()
    {
        $cart = $this->context->cart;
        $total = sprintf(
            $this->l('%1$s (tax incl.)', array(), 'Modules.Faircoinpayment.Shop'),
            Tools::displayPrice($cart->getOrderTotal(true, Cart::BOTH))
        );

        $faircoinAddress = Tools::nl2br($this->address);
        if (!$faircoinAddress) {
            $faircoinAddress = '___________';
        }

        $faircoinReservationDays = Configuration::get('FAIRCOIN_RESERVATION_DAYS');
        if (false === $faircoinReservationDays) {
            $faircoinReservationDays = 7;
        }

        $faircoinCustomText = Tools::nl2br(Configuration::get('FAIRCOIN_CUSTOM_TEXT', $this->context->language->id));
        if (false === $faircoinCustomText) {
            $faircoinCustomText = '';
        }

        return array(
            'total' => $total,
            'faircoinAddress' => $faircoinAddress,
            'faircoinReservationDays' => (int)$faircoinReservationDays,
            'faircoinCustomText' => $faircoinCustomText,
        );
    }
}
